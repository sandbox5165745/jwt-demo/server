const express = require('express')
const router = express.Router()

const BlogController = require('controllers/blog')

const authenticator = require('modules/authenticator')

router.post('/', authenticator.verifyAccessToken, async (req, res) => {
  res.send(await BlogController.add(req.body))
})

router.get('/all', async (req, res) => {
  res.send(await BlogController.get('all'))
})

router.get('/', authenticator.verifyAccessToken, async (req, res) => {
  res.send(await BlogController.get('user', req.body))
})

module.exports = router