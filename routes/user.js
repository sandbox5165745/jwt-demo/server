const express = require('express')
const router = express.Router()

const UserController = require('controllers/user')

router.post('/register', async (req, res) => {
  res.send(await UserController.register(req.body))
})

router.post('/login', async (req, res) => {
  res.send(await UserController.login(req.body))
})

module.exports = router
