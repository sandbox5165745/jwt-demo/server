const Blog = require('models/blog')

exports.add = async (params) => {
  try {
    let blog = new Blog({
      title: params.title,
      content: params.content,
      user: params.userId
    })

    blog.save()

    return {
      status: 'blog_added',
      message: 'The blog has been added to the database.'
    }
  } catch (error) {
    return {
      error: 'operation_failed',
      message: error.message
    }
  }
}

exports.get = async (type, params) => {
  try {
    let query = {}

    if (type === 'user') {
      query = { user: params.userId }
    }

    return await Blog.find(query).populate({
      path: 'user',
      select: 'firstName lastName'
    })
  } catch (error) {
    return {
      error: 'operation_failed',
      message: error.message
    }
  }
}