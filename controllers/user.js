const bcrypt = require('bcrypt')

const authenticator = require('modules/authenticator')

const User = require('models/user')

exports.register = async (params) => {
  try {
    let user = new User({
      firstName: params.firstName,
      lastName: params.lastName,
      email: params.email,
      password: bcrypt.hashSync(params.password, 10)
    })

    user.save()

    return {
      status: 'registration_success',
      message: 'The user has been registered to the database.'
    }
  } catch (error) {
    return {
      error: 'operation_failed',
      message: error.message
    }
  }
}

exports.login = async (params) => {
  try {
    const user = await User.findOne({ email: params.email })

    if (user === null) {
      return {
        error: 'auth_failed',
        message: 'User not found.'
      }
    }

    const passwordMatched = await bcrypt.compare(params.password, user.password)

    if (passwordMatched) {
      return { 
        fullName: `${user.firstName} ${user.lastName}`,
        accessToken: authenticator.createAccessToken(user.toObject())
      }
    } else {
      return {
        error: 'auth_failed',
        message: 'Password is incorrect.'
      }
    }
  } catch (error) {
    return {
      error: 'operation_failed',
      message: error.message
    }
  }
}
