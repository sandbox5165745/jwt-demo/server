const jwt = require('jsonwebtoken')

const secret = process.env.JWT_SECRET

const removeBearerSubstring = (token) => {
  // Removes the 'Bearer ' substring within the token variable.
  return token.slice(7, token.length)
}

exports.createAccessToken = (user) => {
  const data = { id: user._id }
  return jwt.sign(data, secret, { expiresIn: '6h' })
}

exports.verifyAccessToken = (req, res, next) => {
  let token = req.headers.authorization

  if (typeof token !== 'undefined') {
    token = removeBearerSubstring(token)

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          res.send({
            error: 'token_expired',
            message: 'Your session has expired, login again to continue.'
          })
        } else {
          res.send({
            error: 'token_error',
            message: err
          })
        }
      } else {
        const user = jwt.decode(token, { complete: true }).payload
        req.body.userId = user.id
        next()
      }
    })
  } else {
    return res.send({
      error: 'token_missing',
      message: 'Access token is missing from the request.'
    })
  }
}
