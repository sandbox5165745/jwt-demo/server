require('dotenv').config()
require('app-module-path').addPath('./')

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(cors([
  'http://localhost:3000'
]))

app.use('/api/users', require('./routes/user'))
app.use('/api/blogs', require('./routes/blog'))

mongoose.connection.once('open', () => console.log(`API now connected to MongoDB database.`))
mongoose.connect(process.env.MONGODB_URL)

app.listen(process.env.PORT || 4000, () => {
  console.log(`API now online at ${process.env.PORT || 4000}.`)
})
