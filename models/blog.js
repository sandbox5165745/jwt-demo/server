const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const blogSchema = new mongoose.Schema({
  title: {
    type: String,
    unique: true,
    required: [true, 'Blog title is required.']
  },
  content: {
    type: String,
    required: [true, 'Blog content is required.']
  },
  user: {
    type: String,
    required: [true, 'Blog author ID is required.'],
    ref: 'User'
  }
}, {
  timestamps: true
})

blogSchema.plugin(uniqueValidator)
module.exports = mongoose.model('Blog', blogSchema)
